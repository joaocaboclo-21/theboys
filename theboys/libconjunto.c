#include "libconjunto.h"
#include <stdlib.h>
#include <stdio.h>

conjunto_t *cria_cjt (int max){
	
    conjunto_t* cjt = malloc(sizeof *cjt);
    
	if (!cjt) 
		return NULL;
	
	cjt->max = max;
	cjt->card = 0;
	
	if(max) {
		cjt->v = malloc(max * sizeof(int));
		if(!cjt->v)
			return NULL;
	}
	else{
		cjt->v = NULL;
	}
	
	return cjt;
}    


conjunto_t *destroi_cjt (conjunto_t *c){
	
	if(!c)
		return NULL;
		
	if(c->v)
		free(c->v);
		
	free(c);
	
	return NULL;
}

int vazio_cjt (conjunto_t *c){
	
	if(!c)
		return 0;
		
	return !c->card;
}

int cardinalidade_cjt (conjunto_t *c){
	
	if(!c)
		return 0;
	
	return c->card;
}

int insere_cjt (conjunto_t *c, int elemento){
	
	int j;
	
	if(!c || c->card == c->max)
		return 0;
		
	for(j = 0; j < c->card; j++){
		if(elemento == c->v[j])
			return 1;
	}
	
	c->v[c->card++] = elemento;
	
	return 1;
}

void remove_elemento (conjunto_t* c, int ele){

	int i;
	
	for(i = ele; i < c->card - 1; i++)
		c->v[i] = c->v[i + 1];
	
	c->card--;
}

int retira_cjt (conjunto_t *c, int elemento){
	
	int j;
	
	if(!c)
		return 0;
	
	for(j = 0; j < c->card; j++)
	
		if(elemento == c->v[j]){
		
			remove_elemento(c, j);
			return 1;
		}
	return 0;

}

int pertence_cjt (conjunto_t *c, int elemento){

	int i;
	
	if(!c)
		return 0;
		
	for(i = 0; i < c->card; i++){
		
		if(elemento == c->v[i])
			return 1;
	}
	
	return 0;
}

int contido_cjt (conjunto_t *c1, conjunto_t *c2){
	
	int i;
	
	if(!c1 || !c2)
		return 0;
	
	for(i = 0; i < c1->card; i++){
		
		if(!pertence_cjt(c2, c1->v[i]))
			return 0;
	}
	
	return 1;

}

int sao_iguais_cjt (conjunto_t *c1, conjunto_t *c2){
	
	if(!c1 || !c2 || c1->card != c2->card)
		return 0;
		
	return contido_cjt(c1, c2);
	
}

conjunto_t *diferenca_cjt (conjunto_t *c1, conjunto_t *c2){

	int i;

	if(!c1 || !c2)
		return NULL;
		
	conjunto_t *cjt = cria_cjt(c1->max);
	
	if(!cjt)
		return NULL;
		
	for(i = 0; i < c1->card; i++){
		if(!pertence_cjt(c2, c1->v[i]))
			cjt->v[cjt->card++] = c1->v[i];
	}		
			
	return cjt;


}

conjunto_t *interseccao_cjt (conjunto_t *c1, conjunto_t *c2){
	
	int i;
	conjunto_t *cjt = cria_cjt(c1->max);
	
	if(!c1 || !c2)
		return NULL;
		
	for(i = 0; i <  c1->card; i++){
		
		if(pertence_cjt(c2, c1->v[i]))
			cjt->v[cjt->card++] = c1->v[i];
	}		
			
	return cjt;
}

conjunto_t *uniao_cjt (conjunto_t *c1, conjunto_t *c2){
	
	int i;
	
	conjunto_t *cjt = cria_cjt(c2->max), *difi;
	if (!c2)
		return c1;
	
	difi = diferenca_cjt(c2, c1);
	
	for(i = 0; i < c1->card; i++)
		cjt->v[i] = c1->v[i];
		
	cjt->card = c1->card;
	
	for(i = 0; i < difi->card; i++)
		cjt->v[cjt->card++] = difi->v[i];
	
	destroi_cjt(difi);
	return cjt;
}

conjunto_t *copia_cjt (conjunto_t *c){

	if(!c)
		return NULL;

	int i;
	
	conjunto_t *cjt = cria_cjt(c->max);
	
	for(i = 0; i < c->card; i++)
		cjt->v[i] = c->v[i];
		
	cjt->max = c->max;
	cjt->card = c->card;
	cjt->ptr = c->ptr;
	
	return cjt;
}

conjunto_t *cria_subcjt_cjt (conjunto_t *c, int n){

	if(!c)
		return NULL;
	
	if(n >= c->card)
		return copia_cjt(c);
		
	int i = 0, aleatorio = 0;
	
	conjunto_t *cjt = cria_cjt(n);
	
	if(!cjt)
		return NULL;
	
	while(i < n){
	    aleatorio = c->v[rand() % c->card];
	    
	    if (!pertence_cjt(cjt, aleatorio)){
		    cjt->v[i] = aleatorio;
		    cjt->card++;
		    i++;
		    
	    }    
	}
	return cjt;
}

void ordena_cjt(conjunto_t *c){

	int min, temp, i, j;
	
	for(i = 0; i < c->card - 1; i++){
	
		min = i;
	
		for(j = i+1; j < c->card; j++){
			
			if(c->v[min] > c->v[j])
				min = j;
	
		}
		temp = c->v[i];
		c->v[i] = c->v[min];
		c->v[min] = temp;
	}
}

void imprime_cjt (conjunto_t *c){
	
	int i;
	
	if(c->card == 0)
		printf("Conjunto vazio. \n");
		
	ordena_cjt(c);
	
	for(i=0; i < c->card; i++){
		
		printf("%d", c->v[i]);
		
		if(i == c->card - 1)
			printf("\n");
		else
			printf(" ");
	}
}

void inicia_iterador_cjt (conjunto_t *c){

	c->ptr = 0;
	
}

int incrementa_iterador_cjt (conjunto_t *c, int *ret_iterador){

	*ret_iterador = c->v[c->ptr++];
	
	return c->ptr < c->max;
}

int retira_um_elemento_cjt (conjunto_t *c){
	
	int ind, elemento;
	
	ind = rand() % c->card;
	elemento = c->v[ind];
	
	retira_cjt(c, elemento);
	
	return elemento;
}
