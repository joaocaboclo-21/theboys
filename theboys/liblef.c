#include "liblef.h"
#include <stdlib.h>
#include <stdio.h>

lef_t *cria_lef (){
    lef_t *lef = malloc(sizeof *lef);
    nodo_lef_t *novo = malloc(sizeof(nodo_lef_t));

    if(!lef)
        return NULL;
    
    lef->Primeiro = novo;
    lef->Primeiro->prox = NULL;
    lef->Primeiro->evento = NULL;

    return lef;
}

nodo_lef_t *cria_no (evento_t *evento){
    nodo_lef_t *novo = malloc(sizeof(nodo_lef_t));

    if(!novo)
        return NULL;

    novo->evento = evento;
    novo->prox = NULL;

    return novo;
}

lef_t *destroi_lef (lef_t *l){
    if(l->Primeiro != NULL){
        nodo_lef_t *proxNo, *atual;

        atual = l->Primeiro;

        while(atual != NULL){
            proxNo = atual->prox;
            free(atual->evento);
            free(atual);
            atual = proxNo;
        }
    }
    free(l);

    return NULL;
}

int adiciona_inicio_lef (lef_t *l, evento_t *evento){
    
    if(l->Primeiro->evento == NULL){
        l->Primeiro->evento = evento;
        l->Primeiro->prox = NULL;
        return 1;
    }
    
    nodo_lef_t *novo = cria_no(evento);
    nodo_lef_t *antigo = l->Primeiro;

    if(!novo)
        return 0;

    l->Primeiro = novo;
    novo->prox = antigo;

    return 1;

}

int adiciona_ordem_lef (lef_t *l, evento_t *evento){
    
     if(!l)
        return 0;

    if(l->Primeiro->evento == NULL || l->Primeiro->evento->tempo > evento->tempo){
        adiciona_inicio_lef(l, evento);

        return 1;
    }
    else{
        nodo_lef_t *atual = l->Primeiro->prox, *anterior = l->Primeiro;
        nodo_lef_t *novo = cria_no(evento);

        while(anterior->evento->tempo <= novo->evento->tempo && atual != NULL){
            
            if(atual->evento->tempo > novo->evento->tempo)
                break;
            
            anterior = atual;
            atual = atual->prox;
 
        }
        anterior->prox = novo;
        novo->prox = atual;
        
    }
    return 1;
}

evento_t *obtem_primeiro_lef (lef_t *l){
    if(l->Primeiro == NULL){
        return NULL;
        free(l->Primeiro);
    }
    
    nodo_lef_t *noaremover = l->Primeiro;
    l->Primeiro = noaremover->prox;
    evento_t *resposta = noaremover->evento;
    
    free(noaremover);

    return resposta;
}
