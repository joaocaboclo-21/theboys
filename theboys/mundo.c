#include "libconjunto.h"
#include "libfila.h"
#include "liblef.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define CHEGADA 1
#define SAIDA 2
#define MISSAO 3
#define FIM 4
#define FIM_DO_MUNDO 34944
#define CAP_MAX 30
#define CAP_MIN 5


typedef struct Heroi_{
    int id; /*0 - N_HEROIS-1*/
    int paciencia; /*0-100*/
    int idade; /*18-100*/
    int experiencia;
    conjunto_t *cj_habilidades;
}heroi;

typedef struct Locais_{
    int id; /*0 - N_LOCAIS-1*/
    int lotacao_max; /*5-30*/
    int localizacaoX; /*0 - N_LOCAIS-1*/
    int localizacaoY; /*0 - N_LOCAIS-1*/
    conjunto_t *herois_no_lugar;
    fila_t *fila;
}local;

typedef struct Missao_{
    int id;
    conjunto_t *habilidades;
}missao;

typedef struct Mundo_{
    int tempo_de_simulacao;
    int tamanho_mundo;
    int numero_de_herois;
    int numero_de_locais;
    int numero_de_missoes;
    int numero_de_habilidades;
    heroi **N_HEROIS;/*Para armazenar todos os herois*/
    local **N_LOCAIS;/*Para armazenar todas os locais*/
    missao **N_MISSOES;/*Para armazenar todas as missoes*/
    conjunto_t *habilidades_mundo;
    lef_t *lista_futuros;
}mundo;

int max(int minimo, int valor){
    
    if (valor < minimo)
        return minimo;
    
    return valor;
}
    

/*Cria heroi*/
heroi *cria_heroi(conjunto_t *habilidades, int id_anterior){
    
    heroi *hero = malloc(sizeof(heroi));
    
    if (!hero)
        return NULL;
    
    hero->id = id_anterior;
    hero->experiencia = 0;
    hero->idade = rand() % (100 - 18) + 18;
    hero->paciencia = rand() % 100;
    /*Cria subconjunto de habilidades nao repitidas*/
    hero->cj_habilidades = cria_subcjt_cjt(habilidades, rand() % (5 - 2) + 2);

    return hero;
}

/*Cria local*/
local *cria_local(int tamanho_mundo, int id_anterior){
    
    local *loc = malloc(sizeof(local));

    if (!loc)
        return NULL;

    loc->id = id_anterior;
    loc->lotacao_max = rand() % (CAP_MAX - CAP_MIN) + 5;
    loc->localizacaoX = rand() % (tamanho_mundo - 1);
    loc->localizacaoY = rand() % (tamanho_mundo - 1);
    loc->herois_no_lugar = cria_cjt(loc->lotacao_max);
    loc->fila = cria_fila();

    return loc;

}

/*Cria missao*/
missao *cria_missao(conjunto_t *habilidades, int id_anterior){
    missao *mis = malloc(sizeof(missao));
    
    if (!mis)
        return NULL;
        
    mis->id = id_anterior;
    
    /*Ainda nao atribui abilidades ao conjunto*/
    mis->habilidades = NULL;
    
    return mis;
}

/*Inicia Herois no mundo e adiciona ao vetor de herois no mundo*/
heroi **iniciaHerois(mundo *world){
    
    int i;
    heroi **hero = malloc(world->numero_de_herois * sizeof(heroi));

    if(!hero)
        return NULL;

    /*Adiciona a quantidade especificada de herois no mundo*/
    for (i = 0; i < world->numero_de_herois; i++){
        hero[i] = cria_heroi(world->habilidades_mundo, i);
    }

    return hero;
}

/*Inicia locais no mundo e adiciona no vetor herois do mundo*/
local **iniciaLocais(mundo *world){
    
    int i;
    local **loc = malloc(world->numero_de_locais * sizeof(local));

    if (!loc)
        return NULL;

    /*Adiciona a quantidade especificada de locais no mundo*/
    for (i = 0; i < world->numero_de_locais; i++){
        loc[i] = cria_local(world->tamanho_mundo, i);
    }

    return loc;
}

/*Inicia missoes no mundo e adiciona no vetor missoes do mundo*/
missao **iniciaMissoes(mundo *world){
    
    int i;
    missao **mis = malloc(world->numero_de_missoes * sizeof(missao));
    
    if (!mis)
        return NULL;
        
    /*Adiciona a quantidade especificada de missoes no mundo*/    
    for (i = 0; i < world->numero_de_missoes; i++){
        mis[i] = cria_missao(world->habilidades_mundo, i);
    }
    
    return mis;
}

/*Cria mundo seguindo as especificações*/
mundo *criaMundo(int tamanho, int numero_habilidades){
    
    mundo *world = malloc(sizeof(mundo));
    world->habilidades_mundo = cria_cjt(numero_habilidades);
    int i;

    if (!world)
        return NULL;

    world->tempo_de_simulacao = 0;
    world->tamanho_mundo = tamanho;
    world->numero_de_herois = numero_habilidades * 5;
    world->numero_de_locais = (numero_habilidades * 5)/6;
    world->numero_de_missoes = FIM_DO_MUNDO/100;
    world->numero_de_habilidades = numero_habilidades;

    for (i = 1; i <= numero_habilidades; i++)
        insere_cjt(world->habilidades_mundo, i);

    world->N_HEROIS = iniciaHerois(world);
    world->N_LOCAIS = iniciaLocais(world);
    world->N_MISSOES = iniciaMissoes(world);
    world->lista_futuros = cria_lef();

    return world;
} 

/*Cria eventos e posiciona eles no lugar apropriado na lista de eventos futuros*/
int novo_evento(lef_t *l, int dado1, int dado2, int evento, int tempo, int constante){
    
    evento_t *even = malloc(sizeof *even);
    
    if (!even)
        return 0;
    
    even->dado1 = dado1;
    even->dado2 = dado2;
    even->tipo = evento;
    even->tempo = tempo;
    
    /*Se a constante for diferente de 0 o evento ira "furar fila" (utilizado apenas em chegadas)*/
    if (constante == 0)
        adiciona_ordem_lef(l, even);
    
    else
        adiciona_inicio_lef(l, even);
    
    return 1;
}


/*Posiciona herois, missoes e fim*/
int posiciona_inicio(mundo *m){

    int heroi_que_chega, id_missao, tempo_missao, i, id_aleatorio, momento_da_chegada;
    
    /*Posiciona herois em lugares aleatorios no mundo no instante 0*/
    for (i = 0; i < m->numero_de_herois; i++){
        
        heroi_que_chega = m->N_HEROIS[i]->id;
        id_aleatorio = m->N_LOCAIS[rand () % m->numero_de_locais]->id;
        momento_da_chegada = rand () % (70);
        novo_evento(m->lista_futuros, heroi_que_chega, id_aleatorio, CHEGADA, momento_da_chegada, 0);
    }
    
    /*Cria missoes com abilidades requiridas aleatorias e as posiciona em momentos aleatorios na lista de eventos futuros*/
    for (i = 0; i < m->numero_de_missoes; i++){
        
        id_missao = m->N_MISSOES[i]->id;
        tempo_missao = rand () % FIM_DO_MUNDO;
        novo_evento(m->lista_futuros, id_missao, 0, MISSAO, tempo_missao, 0);
    }
    
    /*Cria evento de FIM e o adiciona no momento que deve ser o fim do mundo*/
    novo_evento(m->lista_futuros, 0, 0, FIM, FIM_DO_MUNDO, 0);
    
    return 1;
}

/*Funçao para calcular a distancia entre 2 pontos no plano cartesiano*/
long int calcula_deslocamento(mundo *m, evento_t *evento, int id_aleatorio){
    
     int destinox = m->N_LOCAIS[id_aleatorio]->localizacaoX, destinoy = m->N_LOCAIS[id_aleatorio]->localizacaoY;
     int atualx = m->N_LOCAIS[evento->dado2]->localizacaoX, atualy = m->N_LOCAIS[evento->dado2]->localizacaoY;
     
    long int distancia = sqrt((destinox - atualx)*(destinox - atualx) + (destinoy - atualy)*(destinoy - atualy));
    
    return distancia;
    
}

/*Funcao para tratar os eventos de chegada*/
int trata_chegada(mundo *m, evento_t *evento){
    
    int dado1 = evento->dado1, dado2 = evento->dado2;

    printf("%6d: CHEGA HEROI %2d Local %d ", evento->tempo, dado1, dado2);

    /*No caso do local já estar lotado*/
    if (m->N_LOCAIS[dado2]->lotacao_max <= cardinalidade_cjt(m->N_LOCAIS[dado2]->herois_no_lugar)){
        
        /*Mostra a cardinalidade sem que o id do heroi seja adicionado no conjunto*/
        printf("(%2d/%2d), ",cardinalidade_cjt(m->N_LOCAIS[dado2]->herois_no_lugar), m->N_LOCAIS[dado2]->lotacao_max);
        /*No caso do heroi possuir paciencia o suficiente ele é inserido na fila do local*/
        if ((m->N_HEROIS[dado1]->paciencia)/4 - tamanho_fila(m->N_LOCAIS[dado2]->fila) > 0){
            
            insere_fila(m->N_LOCAIS[dado2]->fila, dado1);
            printf("FILA %2d\n", tamanho_fila(m->N_LOCAIS[dado2]->fila));
        }
        
        /*No caso do heroi não possuir a paciencia suficiente e desitira de entrar no local e automaticamente cria um evento de saida*/
        else{
            
            printf("DESISTE\n");
            novo_evento(m->lista_futuros, dado1, dado2, SAIDA, m->tempo_de_simulacao, 0);
        }
        return 1;
    }
    /*No caso do local não estar lotado*/
 
    /*Tempo de permanencia do heroi no local*/
    int tpl;
    int aleat = rand() % (6 + 2) - 2;
    tpl = max(1, (m->N_HEROIS[dado1]->paciencia)/10 + aleat);
        
    insere_cjt(m->N_LOCAIS[dado2]->herois_no_lugar, dado1);
        
    /*Cria evento de saida do heroi para depois do tempo de permanencia desse no local*/
    novo_evento(m->lista_futuros, dado1, dado2, SAIDA, m->tempo_de_simulacao + tpl, 0); 

    /*Mostra a ardinalidade apos o id do heroi ser adicionado no conjunto*/
    printf("(%2d/%2d), ENTRA\n", cardinalidade_cjt(m->N_LOCAIS[dado2]->herois_no_lugar), m->N_LOCAIS[dado2]->lotacao_max);

    return 1;
} 

/*Função para tratar eventos de saida*/
int trata_saida(mundo *m, evento_t *evento){
    
    int tdl, retirado, tevefila = 0, dado1 = evento->dado1, dado2 = evento->dado2;
    int id_aleatorio = m->N_LOCAIS[rand () % m->numero_de_locais]->id;


    retira_cjt (m->N_LOCAIS[dado2]->herois_no_lugar, dado1);
    
    printf("%6d: SAIDA HEROI %2d Local %d (%2d/%2d)", evento->tempo, dado1, dado2, cardinalidade_cjt(m->N_LOCAIS[dado2]->herois_no_lugar), m->N_LOCAIS[dado2]->lotacao_max);    
    
    /*No caso do heroi abrir espaço no local e existir herois na fila é criado evento de chegada instantaneo para o heroi da fila*/
    if (!vazia_fila(m->N_LOCAIS[dado2]->fila) && m->N_LOCAIS[dado2]->lotacao_max > cardinalidade_cjt(m->N_LOCAIS[dado2]->herois_no_lugar)){

        /*teve fila sinalizara que um heroi foi removido da fila do local*/
        retira_fila(m->N_LOCAIS[dado2]->fila, &retirado);
        tevefila = 1;
        novo_evento(m->lista_futuros, retirado, dado2, CHEGADA, m->tempo_de_simulacao, 1);
    }
    
    /*Calcula a distancia entre dois pontos(long int pois leva em conta a maior distancia possivel no mundo)*/
    long int distacia = calcula_deslocamento(m, evento, id_aleatorio);
   
    /*Para garantir que a velociadade maxima de deslocamento seja 100*/
    int velocidade = 100 - max(0, m->N_HEROIS[dado1]->idade - 40);

    /*Calcula o tempo de deslocamento*/    
    tdl = (distacia/velocidade)/15; 

    /*Printa mensagem diferente caso um heroi da fila tenha entrado no local*/
    if (tevefila)
        printf(", REMOVE FILA HEROI %2d\n", retirado);
    printf("\n");
    
    /*Cria evento de chegada para depois do tempo de deslocamento do heroi*/
    novo_evento(m->lista_futuros, dado1, id_aleatorio, CHEGADA, m->tempo_de_simulacao + tdl, 0);
    
    return 1;
}

/*Funçao para adiquirir a uniao de habilidades de todos os herois do local*/
conjunto_t *habilidades_local(int local, mundo *m){
    
    int i, j;
    conjunto_t *conjunto = cria_cjt(m->numero_de_habilidades);
    
    /*Para analisar todos os herois do local*/
    for (i = 0; i < cardinalidade_cjt(m->N_LOCAIS[local]->herois_no_lugar); i++){
        
        /*Para adicionar o id dos herois do local em um conjunto auxiliar*/
        for (j = 0; j < cardinalidade_cjt(m->N_HEROIS[m->N_LOCAIS[local]->herois_no_lugar->v[i]]->cj_habilidades); j++){
            /*insere a abilidade do heroi em um conjunto*/
            insere_cjt(conjunto, m->N_HEROIS[m->N_LOCAIS[local]->herois_no_lugar->v[i]]->cj_habilidades->v[j]);
        }

    }
    return conjunto;
}

/*Funçao para atribuir xp a herois após a missão*/
int atribui_xp(mundo *m, int equipe){

    int j;
    
    for (j = 0; j < cardinalidade_cjt(m->N_LOCAIS[equipe]->herois_no_lugar); j++)
            m->N_HEROIS[m->N_LOCAIS[equipe]->herois_no_lugar->v[j]]->experiencia++;
    
    return 1;
}

/*Funçao para tratar missoes do mundo*/
int trata_missao(mundo *m, evento_t *evento){
    
    int i, possivel = 0, equipe, tempo_missao = 0, menor_card = CAP_MAX, dado1 = evento->dado1; /*30 = capacidade maxima de um local*/
    conjunto_t *conjunto;
    m->N_MISSOES[dado1]->habilidades = cria_subcjt_cjt(m->habilidades_mundo, rand () % (6 - 3) + 3);
    
    /*Printa as habilidades requiridas da missao*/
    printf("%6d: MISSAO %3d HAB_REQ ", evento->tempo, evento->dado1);
    imprime_cjt(m->N_MISSOES[dado1]->habilidades);
    
    /*Analisa a combinação de habilidades de todos os locais*/
    for (i = 0; i < m->numero_de_locais; i++){
        
        /*Printa a combinaçao de habilidades de um local*/
        printf("%6d: MISSAO %3d HAB_EQL %d: ", evento->tempo, dado1, i);
        conjunto = habilidades_local(i, m);
        imprime_cjt(conjunto);
        
        /*Analisa a equipe com o menor numero de herois capaz de resolver a missao, atribui 1 a variavel possivel para indecar que ao menos uma equipe pode completar a missao*/
        if(!vazio_cjt(conjunto) && contido_cjt(m->N_MISSOES[dado1]->habilidades, conjunto) && cardinalidade_cjt(conjunto) < menor_card){
            menor_card = cardinalidade_cjt(m->N_LOCAIS[i]->herois_no_lugar);
            equipe = i;
            possivel = 1;
            
        }
        destroi_cjt(conjunto);
    }
    destroi_cjt(m->N_MISSOES[dado1]->habilidades);
    
    /*Caso nenhuma combinaçao de habilidades e de nenhum local seja compativel com a missao*/
    if (!possivel){
        
        printf("%6d: MISSAO %3d IMPOSSIVEL\n", evento->tempo, dado1);
        tempo_missao = rand () % (FIM_DO_MUNDO - m->tempo_de_simulacao) + m->tempo_de_simulacao;
        
        /*Adiciona o evento da missao em um tempo do futuro na lista de eventos futuros*/
        if (m->tempo_de_simulacao < FIM_DO_MUNDO - 1)
            novo_evento(m->lista_futuros, dado1, 0, MISSAO, tempo_missao, 0);

        return 1;    
        
    }
    
    /*Printa a equipe mais apropriada para resolver a missao*/
    printf("%6d: MISSAO %3d HAB_EQS %d: ", evento->tempo, dado1, equipe);
    imprime_cjt(m->N_LOCAIS[equipe]->herois_no_lugar);
    atribui_xp(m, equipe);
    return 1;
    
}

/*Funçao para tratar o fim do mundo*/
int trata_fim(mundo *m, evento_t *evento){
    
    int i;
    
    printf("%6d: FIM\n", evento->tempo);
    printf("--------------------\n");
    
    /*Printa a experiencia que cada heroi adiquiriu durante a resolução das missoes*/
    for (i = 0; i < m->numero_de_herois; i++){
        printf("HEROI %2d EXPERIENCIA %2d\n", m->N_HEROIS[i]->id, m->N_HEROIS[i]->experiencia);
    }
    
    return 1;
}

/*Função para liberar todos os mallocs realizados pelo mundo*/
mundo *destroi_mundo(mundo *m){
    destroi_cjt(m->habilidades_mundo);
    destroi_lef(m->lista_futuros);

    int i;

    for (i = 0; i < m->numero_de_herois; i++){
        destroi_cjt(m->N_HEROIS[i]->cj_habilidades);
        free(m->N_HEROIS[i]);
    }
    
    free(m->N_HEROIS);

    for (i = 0; i < m->numero_de_locais; i++){
        destroi_cjt(m->N_LOCAIS[i]->herois_no_lugar);
        destroi_fila(m->N_LOCAIS[i]->fila);
        free(m->N_LOCAIS[i]);
    }    

    free(m->N_LOCAIS);
    
    for (i = 0; i < m->numero_de_missoes; i++){
        free(m->N_MISSOES[i]);
    }
    
    free(m->N_MISSOES);
    
    free(m);
    return NULL;
}


int main(){

    srand(100);
    int finaliza = 0;
    evento_t *evento;
    
    mundo *m = criaMundo(20000, 10);

    posiciona_inicio(m);
    
    while (!finaliza){
        evento = obtem_primeiro_lef(m->lista_futuros);
        m->tempo_de_simulacao = evento->tempo;
        
        switch(evento->tipo){
            case CHEGADA:
                trata_chegada(m, evento);
                break;
            
            case SAIDA:
                trata_saida(m, evento);
                break;
                
            case MISSAO:
                trata_missao(m, evento);
                break;
                
            case FIM:
                finaliza = 1;
                trata_fim(m, evento);
                break;
        }
        free(evento);
        
    }
    m = destroi_mundo(m);
    return 1;

}
