#include "libfila.h"
#include <stdlib.h>
#include <stdio.h>


fila_t *cria_fila (){
    fila_t *f = malloc(sizeof *f);

    if(!f)
        return NULL;
    
    f->ini = NULL;    
    f->fim = NULL;
    f->tamanho = 0;

    return f;
}

fila_t *destroi_fila (fila_t *f){
	
	if(!f)
		return NULL;
	
    nodo_f_t *aux;
    
    while (f->ini != NULL){
        aux = f->ini;
        f->ini = f->ini->prox;
        free(aux);
    } 
    
    free(f);
	
	return NULL;
}

int vazia_fila (fila_t *f){
    if(!f)
        return 1;
    
    return !f->ini;
}

int tamanho_fila (fila_t *f){
    if(!f)
        return 0;

    return f->tamanho;
}

int insere_fila (fila_t *f, int elemento){
    nodo_f_t *novo = (nodo_f_t*)malloc(sizeof(nodo_f_t));
    if(!novo || !f)
        return 0;

    novo->elem = elemento;
    novo->prox = NULL;

    if(vazia_fila(f)){
        f->ini = novo;
    }

    else{
        f->fim->prox = novo;
    }
    f->fim = novo;
    f->tamanho++;
        
    return 1;
}

int retira_fila (fila_t *f, int *elemento){
    nodo_f_t *remover = NULL;

    if(!f || !f->ini)
        return 0;

    remover = f->ini;
    f->ini = remover->prox;
    f->tamanho--;

    *elemento = remover->elem;
    free(remover);

    return 1;

}

void imprime_fila (fila_t *f){
    nodo_f_t *aux = f->ini;
    while(aux){
        printf("%d ", aux->elem);
        aux = aux->prox;
    }
    printf("\n");
    free(aux);

}

